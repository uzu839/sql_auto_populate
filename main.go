//automates the sign up process inserting a users First name, last name, created username and password into a MySQL users table

package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime"
	"sync"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

var fname string
var lname string
var password string
var names []string

var db *sql.DB
var err error
var dbHost = "127.0.0.1:3306"
var dbName = "test_db"

const (
	O_APPEND int = syscall.O_APPEND
	O_WRONLY int = syscall.O_WRONLY
	O_CREATE int = syscall.O_CREAT
)

func main() {

	var dbLog, _ = os.OpenFile("mysql.log", O_WRONLY|O_CREATE|O_APPEND, 0755)
	defer dbLog.Close()
	log.SetOutput(dbLog)

	db, err = sql.Open("mysql", "root:1234567@tcp("+dbHost+")/"+dbName)
	if err != nil {
		log.Println("Database connection not available", err)
		fmt.Println(err)
	}
	defer db.Close()

	//Create the waitgroup
	var wg sync.WaitGroup

	runtime.GOMAXPROCS(runtime.NumCPU())
	//Infinite for loop to run the signUp function
	for {
		//Tell the program to sleep for 250 Milliseconds after each run
		time.Sleep(250 * time.Millisecond)
		//Increment the WaitGroup counter by 1
		wg.Add(1)
		//Run go routine --dereference the waitgroup variable
		go signUP(&wg)
	}

	wg.Wait()

	//Channel version of code
	/*channel := make(chan bool)
	go signUP(channel)
	<-channel*/
}

//Channel version of code
//--func signUP(c chan bool)
//takes in a pointer to sync.WaitGroup
//WHY -- only *Wait.Group(s) have the Done() method
func signUP(wg *sync.WaitGroup) {

	defer wg.Done()

	db, err = sql.Open("mysql", "root:1234567joelaine@tcp("+dbHost+")/"+dbName)
	if err != nil {
		log.Println("Database connection not available", err)
		fmt.Println(err)
	}
	defer db.Close()

	//Generate Random User Info
	names := []string{
		"James", "Michael", "Robert", "John", "David", "William", "Richard", "Thomas", "Mark", "Charles", "Steven", "Gary", "Joseph", "Donald", "Ronald", "Kenneth", "Paul", "Larry", "Daniel", "Stephen", "Dennis", "Timothy", "Edward", "Jeffrey", "George", "Gregory", "Kevin", "Douglas", "Terry", "Anthony", "Mary", "Linda", "Patricia", "Susan", "Deborah", "Barbara", "Debra", "Karen", "Nancy", "Donna", "Cynthia", "Sandra", "Pamela", "Sharon", "Kathleen", "Carol", "Diane", "Brenda", "Cheryl", "Janet", "Elizabeth", "Kathy", "Margaret", "Janice", "Carolyn", "Denise", "Judy", "Rebecca", "Joyce", "Teresa"}

	namesLength := len(names)
	rand.Seed(time.Now().UnixNano())

	randomNumber := rand.Intn(namesLength)
	fname := names[randomNumber]

	randomNumber2 := rand.Intn(namesLength)
	lname := names[randomNumber2]

	randomNumber3 := rand.Intn(namesLength)
	password := names[randomNumber3]

	m := map[string]string{
		"fname":    fname,
		"lname":    lname,
		"username": userName(fname, lname),
		"password": encryptPass(password),
	}

	fmt.Println(m)

	//SQL query to insert values into the table
	stmt, err := db.Query("INSERT INTO users VALUES (default, " + "'" + m["fname"] + "', " + "'" + m["lname"] + "', " + "'" + m["username"] + "', " + "'" + m["password"] + "')")
	if err != nil {
		log.Println(err)
		fmt.Println(err)
	}
	defer stmt.Close()

	//channel version of code
	//c <- true

}

func userName(f string, l string) string {

	userName := string(f[0]) + l
	fmt.Println(f)
	fmt.Println(l)
	fmt.Println(userName)
	return userName
}

func encryptPass(s string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
	}
	return string(hash)
}
