module gitlab.com/uzu839/auto_user

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392
)
